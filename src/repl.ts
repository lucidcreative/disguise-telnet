import * as repl from 'repl'
import { DisguiseManager } from './disguiseManager'
const replServer = repl.start({ useColors: true })

Object.assign(replServer.context,
	{ d3: new DisguiseManager({ ip: '127.0.0.1', statusCallback: console.log }) })
