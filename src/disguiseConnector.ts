import * as carrier from 'carrier'
import { Socket } from 'net'
import { has, prop } from 'ramda'
import { inspect } from 'util'

interface DisguiseTelnetResponse {
	request: number
	status: string,
	results?: any,
}

export const enum Status {
	CLOSED = 'CLOSED',
	ACTIVE = 'ACTIVE',
	CHANGING = 'CHANGING',
	ERROR = 'ERROR',
}

export class DisguiseConnector {
	private socket: Socket
	// this is a dictionary of the ids that are out on request,
	// and promise resolvers to call when the socket gets a matching id in return
	private openTasks
	private statusCallback: (status: Status) => void

	host: string
	port: number

	constructor({ host, port = 54321 }, statusCallback: (status: Status) => void) {
		this.host = host
		this.port = port
		this.openTasks = {}
		this.statusCallback = statusCallback
		this.socket = new Socket()
		carrier.carry(this.socket, this.receieve)
		// status callbacks
		this.socket.on('error', () => {
			this.statusCallback(Status.ERROR)
		})
		this.socket.on('close',
			(hadError) => this.statusCallback(hadError ? Status.ERROR : Status.CLOSED))
		this.socket.on('end', () => this.statusCallback(Status.CHANGING))
		this.socket.on('connect', () => this.statusCallback(Status.ACTIVE))
		this.socket.on('timeout', () => this.statusCallback(Status.ERROR))
	}

	connect = (): Promise<void> => {
		this.statusCallback(Status.CHANGING)
		return new Promise((resolve, reject) => {
			try {
				this.socket.connect({
					host: this.host,
					port: this.port,
				}, () => {
					resolve()
				})
			} catch (e) {
				this.statusCallback(Status.ERROR)
				reject(e)
			}
		})
	}

	close = () => {
		this.statusCallback(Status.CHANGING)
		this.socket.end('', () => this.statusCallback(Status.CLOSED))
	}

	send = (data: object): Promise<object> => {
		// check the connection
		if (!this.socket.writable) { throw Error('connection is not open!') }

		// build a promise around the send, to resolve when the socket receives a packet with the id
		return new Promise((resolve, reject) => {
			const packet = this.bundlePacket(data)
			this.openTasks[`${packet.request}`] = { resolve, reject }
			try {
				this.socket.write(packet.data)

			} catch (e) {
				reject(e)
			}
		})
	}

	private receieve = (data: Buffer) => {
		// todo(kb): add handler in case one message gets broken across multiple sends

		const parsed = JSON.parse(data.toString()) as DisguiseTelnetResponse
		const request = prop('request', parsed)
		const result = prop('results', parsed)
		if (request && has(`${request}`, this.openTasks)) {
			if (parsed.status === 'OK') {
				this.openTasks[request].resolve(result)
			} else {
				this.openTasks[request].reject(`result was an error: ${inspect(parsed, false, 3, true)}`)
			}
			delete this.openTasks[request]
		}
	}

	isConnected = () => {
		return this.socket.writable
	}

	private bundlePacket = (data: object) => {
		const request = Math.floor(Math.random() * 1000000)
		return {
			data: JSON.stringify({ ...data, request }) + ' \n',
			request,
		}
	}
}
