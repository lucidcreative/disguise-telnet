import * as carrier from 'carrier'
import { Server, Socket } from 'net'
import { pathOr, prop } from 'ramda'
import {
	cuesForTrack1,
	cuesForTrack2,
	cuesForTrack3,
	playerList,
	trackList,
} from './testResponse.data'

export interface ResponseBundle {
	data: object,
	request: number,
	delay?: number
}

export class MockServer {

	static parse(data: Buffer): ResponseBundle {
		try {
			const parsed = JSON.parse(data.toString())
			if (typeof parsed.request !== 'number') { throw Error('request is not a valid number') }
			console.log(parsed)
			const q = pathOr(false, ['query', 'q'], parsed)
			const trackCommand = pathOr(false, ['track_command'], parsed)
			if (q) {
				const queries = {
					trackList,
					'cueList "track 1"': cuesForTrack1,
					'cueList "track 2"': cuesForTrack2,
					'cueList "track 3"': cuesForTrack3,
					playerList,
				}
				return {
					request: parsed.request,
					data: queries[q],
					delay: prop('delay', parsed),
				}
			} else if (trackCommand) {
				// for now, just echo
				return {
					request: parsed.request,
					data: { status: 'OK' },
					delay: prop('delay', parsed),
				}
			} else {
				throw Error()
			}

		} catch (e) {
			console.log(`bad packet ${e}: ${data}`)
			return {
				request: -1,
				data: { status: 'Failed to parse.' },
			}
		}
	}

	server: Server
	clients: Set<Socket> = new Set<Socket>()

	socketSend = (socket: Socket, requestId: number, data: object) => {
		socket.write(JSON.stringify({
			...data,
			request: requestId,
		}) + '\n')
	}

	sendData = (socket: Socket) => (packet: Buffer) => {
		const { data, request, delay } = MockServer.parse(packet)
		setTimeout(() => this.socketSend(socket, request, data), delay || 32)
	}

	closeConnections = () => {
		this.clients.forEach((client: Socket) => {
			client.end('unexpected error')
			client.destroy()
			this.clients.delete(client)
		})
	}

	constructor(port: number, ip?: string) {
		this.server = new Server((socket) => {
			this.clients.add(socket)
			console.log('new client!')
			carrier.carry(socket, this.sendData(socket))
			socket.on('close', () => {
				this.clients.delete(socket)
				socket.destroy()
			})
		})

		this.server.on('listening',
			() => console.log(`📟  mock disguise telnet server listening @ ${ip ? ip : '0.0.0.0'}:${port}`))

		this.server.listen(port, ip)
	}
}
