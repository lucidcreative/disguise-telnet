export const trackList = {
	'status': 'OK',
	'results': [
		{ 'track': 'track 3', 'length': 560.0 },
		{ 'track': 'track 2', 'length': 560.0 },
		{ 'track': 'track 1', 'length': 560.0 },
	],
}

export const playerList = {
	'status': 'OK',
	'results': [
		{ 'player': 'player 1' },
		{ 'player': 'player 2' },
	],
}

export const cuesForTrack1 = {
	'status': 'OK',
	'results': [
		{ 'location': '[1] Track 1 Q1', 'startTime': 0.0, 'length': 31.0 },
		{ 'location': '[2] Track 1 Q2', 'startTime': 31.0, 'length': 78.0 },
		{ 'location': '[3] Track 1 Q3', 'startTime': 109.0, 'length': 65.0 },
	],
}

export const cuesForTrack2 = {
	'status': 'OK',
	'results': [
		{ 'location': '[4] Track 2 Q1', 'startTime': 0.0, 'length': 31.0 },
		{ 'location': '[5] Track 2 Q2', 'startTime': 31.0, 'length': 78.0 },
		{ 'location': '[6] Track 2 Q3', 'startTime': 109.0, 'length': 65.0 },
		{ 'location': '[7] Track 2 Q4', 'startTime': 174.0, 'length': 35.0 },
	],
}

export const cuesForTrack3 = {
	'status': 'OK',
	'results': [
		{ 'location': '[8] Track 3 Q1', 'startTime': 0.0, 'length': 31.0 },
		{ 'location': '[9] Track 3 Q2', 'startTime': 31.0, 'length': 78.0 },
		{ 'location': '[10] Track 3 Q3', 'startTime': 109.0, 'length': 65.0 },
		{ 'location': '[11] Track 3 Q4', 'startTime': 174.0, 'length': 35.0 },
		{ 'location': 'Track 3 Q5', 'startTime': 209.0, 'length': 351.0 },
	],
}
