import { DisguiseConnector, Status } from './disguiseConnector'

interface Track {
	track: string,
	length: number,
}

export interface Player {
	player: string,
	// name: string,
}

export interface TrackWithCues extends Track {
	cues: Cue[],
}

export interface Cue {
	id: string,
	location: string,
	startTime: number,
	length: number,
	trackId: string
}

export const enum PlayMode {
	Play = 'play',
	PlaySection = 'playSection',
	Pause = 'pause',
	Stop = 'stop',
}

export class DisguiseManager {
	private _tracks: TrackWithCues[] = []
	private _players: Player[] = []
	private _d3: DisguiseConnector
	private _status: Status = Status.CLOSED
	private _statusCallback
	private _autoReconnect: boolean
	private _shouldBeConnected: boolean = false
	private _retryTime: number
	private _reconnectCallback: NodeJS.Timeout | null

	constructor(
		{ ip,
			port,
			autoConnect = true,
			statusCallback,
			reconnectOnFail: autoReconnect = true,
			retryTime = 5000,
		}:
			{
				ip: string,
				port?: number,
				autoConnect?: boolean,
				statusCallback: (status: any) => void,
				reconnectOnFail?: boolean,
				retryTime?: number,
			},
	) {
		this._statusCallback = statusCallback
		this._reconnectCallback = null
		this._retryTime = retryTime
		this._autoReconnect = autoReconnect
		this._d3 = new DisguiseConnector({ host: ip, port }, this._handleConnectionStatusChange)
		if (autoConnect) { this.connect() }
	}

	async connect(autoRefresh = true) {
		this._shouldBeConnected = true
		if (this._status === Status.ACTIVE) { return }
		this._setStatus(Status.CHANGING)
		this._reconnectCallback = null
		try {
			await this._d3.connect()
			if (autoRefresh) {
				// populate the data structure once
				await this.refreshAllData()
			}
		} catch (e) {
			this._handleConnectionStatusChange(Status.ERROR)
		}
	}

	async disconnect() {
		// remove callbacks
		this._shouldBeConnected = false
		if (this._reconnectCallback) {
			clearTimeout(this._reconnectCallback)
		}
		await this._d3.close()
		this._setStatus(Status.CLOSED)
	}

	async trackCommand({ command, player, transition, cue, cueId }:
		{ command: PlayMode, player: string, transition?: number, cue?: Cue, cueId?: string },
	) {
		const targetCue = cue || cueId !== undefined ? await this.getCueById(cueId!) : undefined
		const { startTime, trackId } = targetCue || { startTime: undefined, trackId: undefined }
		await this._d3.send({
			track_command: {
				command,
				player,
				transition,
				location: `${startTime}`,
				track: trackId,
			},
		})
		return true
	}

	// public data queries

	getStatus(): Status {
		return this._status
	}

	async getPlayers(fromCache: boolean = true) {
		if (!fromCache && this._status === Status.ACTIVE) { await this.refreshPlayers() }
		return this._players
	}

	async getTracks(fromCache: boolean = true): Promise<TrackWithCues[]> {
		if (!fromCache && this._status === Status.ACTIVE) { await this.refreshAllData() }
		return this._tracks
	}

	async getTrackById(id: string, fromCache: boolean = true): Promise<TrackWithCues> {

		// check for trackId first, avoid updating cache if bad id.
		if (!this._tracks.map(t => t.track).includes(id)) {
			throw Error(`Couldn't find Track with id ${id}`)
		}

		// update cache if requested
		if (!fromCache && this._status === Status.ACTIVE) {
			await this.refreshTracks()
			await this.refreshTrackCues(id)
		}

		// get whole track object
		const track = this._tracks.find(t => t.track === id)
		// it's possible the track disappeared in the cache update, so check first
		if (!track) { throw Error(`Couldn't find Track with id ${id}`) }
		return track
	}

	async getCueById(cueId: string, fromCache: boolean = true) {
		const trackId = this._getTrackIdFromCueId(cueId)
		const track = this._tracks.find(t => t.track === trackId)
		if (!track) { throw Error(`Track containing cue ${cueId} could not be found`) }
		if (!fromCache && this._status === Status.ACTIVE) { await this.refreshTrackCues(trackId) }
		return track.cues.find(cue => cue.id === cueId)
	}

	// refreshers

	async refreshAllData() {
		// refresh tracks
		await this.refreshTracks()
		// refresh the cues for each
		await Promise.all(this._tracks.map(t => this.refreshTrackCues(t.track)))
		// refresh players
		// this goes last in case it fails.
		await this.refreshPlayers()
	}

	async refreshTracks(): Promise<boolean> {
		// this refreshes track data WITHOUT updating the nested cues
		const trackData = await this._queryTracks()
		// pull old cue data from tracks
		const mergedTracks = await Promise.all(trackData.map(async newTrack => {
			const oldTrack = this._tracks.find(old => old.track === newTrack.track)
			return {
				...(oldTrack || { cues: await this._queryCuesForTrack(newTrack.track) }),
				...newTrack,
			}
		}))
		this._tracks = mergedTracks
		return true
	}

	async refreshTrackCues(trackId: string): Promise<boolean> {
		const track = this._tracks.find(t => t.track === trackId)
		if (!track) { throw Error(`Track ${trackId} not found`) }
		track.cues = await this._queryCuesForTrack(trackId)
		return true
	}

	async refreshPlayers(): Promise<boolean> {
		this._players = await this._queryPlayers()
		return true
	}

	// queries

	private async _queryTracks(): Promise<Track[]> {
		if (this._status !== Status.ACTIVE) { throw Error(`Cannot query, not connected!`) }
		return await this._d3.send({
			query: {
				q: 'trackList',
			},
		}) as Track[]
	}

	private async _queryPlayers(): Promise<Player[]> {
		if (this._status !== Status.ACTIVE) { throw Error(`Cannot query, not connected!`) }
		return await this._d3.send({
			'query': { 'q': 'playerList' },
		}) as Player[]
		return []
	}

	private async _queryCuesForTrack(trackId: string): Promise<Cue[]> {
		if (this._status !== Status.ACTIVE) { throw Error(`Cannot query, not connected!`) }
		const cues = await this._d3.send(this._cueQuery(trackId)) as Cue[]
		return cues.map(cue => ({ ...cue, trackId: trackId, id: `${trackId}|${cue.startTime}` }))
	}

	private _cueQuery(trackId: string) {
		return {
			query: {
				q: `cueList "${trackId}"`,
			},
		}
	}

	// helpers

	private _getTrackIdFromCueId(cueId: string): string {
		const match = cueId.match(/([^|]*)\|(.*)/)
		if (!match) { throw Error(`${cueId} is not a valid Cue ID`) }
		return match[1]
	}

	private _setStatus(status: Status) {
		if (status !== this._status) {
			// only fire when status is different
			this._statusCallback(status)
		}
		this._status = status
	}

	private _handleConnectionStatusChange = (status: Status) => {
		// update the manager status
		this._setStatus(status)

		// check to see if we need to do a reconnect
		if (this._autoReconnect &&
			this._shouldBeConnected &&
			!this._reconnectCallback &&
			(status === Status.ERROR || status === Status.CLOSED)
		) {
			// if we should be connected but we're not, this is an error state
			this._setStatus(Status.ERROR)
			// TODO: change to managable logger
			console.log(`Disguise Manager: Reconnecting in ${this._retryTime} ms...`)
			this._reconnectCallback = setTimeout(() => this.connect(), this._retryTime)
		}
	}
}
