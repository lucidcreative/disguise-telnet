import { DisguiseManager, Status } from '.'
import { MockServer } from './mockServer'
import {
	cuesForTrack1,
	cuesForTrack2,
	cuesForTrack3,
	trackList,
} from './mockServer/testResponse.data'

const ip = '127.0.0.1'
const port = 54327

describe('integration test with mockServer', () => {

	let mockServer: MockServer
	beforeAll(async () => {
		// start mock server
		mockServer = new MockServer(port, ip)
	})
	afterAll(() => {
		mockServer.server.close()
	})
	it('works', async () => {
		jest.setTimeout(30000)
		const statusCallback = jest.fn()
		const d3 = new DisguiseManager(
			{ ip, statusCallback, autoConnect: false, port })
		expect(statusCallback).toHaveBeenCalledTimes(0)
		await d3.connect()
		expect(statusCallback).toHaveBeenCalledTimes(2)
		expect(statusCallback).toHaveBeenLastCalledWith(Status.ACTIVE)
		const tracks = await d3.getTracks()
		expect(tracks).toHaveLength(trackList.results.length)
		const track1 = tracks.find(x => x.track.includes('1'))
		expect(track1).toBeTruthy()
		expect(track1!.cues.length).toBe(cuesForTrack1.results.length)
		const track2 = tracks.find(x => x.track.includes('2'))
		expect(track2).toBeTruthy()
		expect(track2!.cues).toHaveLength(cuesForTrack2.results.length)
		const track3 = tracks.find(x => x.track.includes('3'))
		expect(track3).toBeTruthy()
		expect(track3!.cues).toHaveLength(cuesForTrack3.results.length)

		await d3.disconnect()
	})
})
