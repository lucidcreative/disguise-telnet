# disguise-telnet
a stateful client for the [disguise](https://www.disguise.one "Disguise") Multitransport Telnet Remote Transport

brought to you by [Lucid](http://lucid.rocks "Lucid").

## usage
basic example
```typescript
import { DisguiseManager, PlayMode } from 'disguise-telnet'

// everything in this library uses async, mostly, so wrap in a function
async function(){
	const d3 = new DisguiseManager({
		ip:'127.0.0.1',

		// include a list of the players 
		// (aka transports) in the multitransport,
		// we can't query these from disguise yet...
		players:[
			'foreground',
			'background'
		]

		// callback function for status changes
		statusCallback: console.log

		// there's other options, use typescript to find them
		})

	const tracks = await d3.getTracks()
	const players = await d3.getPlayers()

	// this will do a 2 sec crossfade to the first cue 
	// of the first track and play section
	d3.trackCommand({
		command: PlayMode.PlaySection,
		player:players[0],
		transition: 2,
		cue: tracks[0].cues[0]
	})
}()
```

## hints
for all of the `get...` functions, there's an optional boolean parameter `fromCache` that defaults to true. you should either periodically run `DisguiseManager#refreshAllData()` or get data with `fromCache` set to false, in order to get updates after the initial connection. The primary goal of this is to avoid over-querying a running master instance by accident

### license
Copyright (c) 2019 Kyle Bjordahl + Lucid Creative LLC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
